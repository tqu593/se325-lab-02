package se325.lab02.concert.services;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * JAX-RS application subclass for the Concert Web service.
 * The base URI for the Concert Web service is:
 * http://<host-name>:<port>/services
 *
 */
@ApplicationPath("/services")
public class ConcertApplication extends Application {

    private Set<Object> _singletons = new HashSet<>();
    private Set<Class<?>> _classes = new HashSet<>();

    public ConcertApplication() {
        _singletons.add(new ConcertResource());
        _classes.add(SerializationMessageBodyReaderAndWriter.class);
    }
    
    @Override
    public Set<Object> getSingletons() {
        // Return a Set containing an instance of ConcertResource that will be
        // used to process all incoming requests on Concert resources.
        return _singletons;
    }
    
    @Override
    public Set<Class<?>> getClasses() {
      return _classes;
    }
}