package se325.lab02.concert.services;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.ws.rs.*;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import se325.lab02.concert.common.Config;
import se325.lab02.concert.domain.Concert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to implement a simple REST Web service for managing Concerts.
 * <p>
 * ConcertResource implements a WEB service with the following interface:
 * - GET    <base-uri>/concerts/{id}
 * Retrieves a Concert based on its unique id. The HTTP response
 * message has a status code of either 200 or 404, depending on
 * whether the specified Concert is found.
 * <p>
 * - GET    <base-uri>/concerts?start&size
 * Retrieves a collection of Concerts, where the "start" query
 * parameter identifies an index position, and "size" represents the
 * max number of successive Concerts to return. The HTTP response
 * message returns 200.
 * <p>
 * - POST   <base-uri>/concerts
 * Creates a new Concert. The HTTP post message contains a
 * representation of the Concert to be created. The HTTP Response
 * message returns a Location header with the URI of the new Concert
 * and a status code of 201.
 * <p>
 * - DELETE <base-uri>/concerts
 * Deletes all Concerts, returning a status code of 204.
 * <p>
 * Where a HTTP request message doesn't contain a cookie named clientId
 * (Config.CLIENT_COOKIE), the Web service generates a new cookie, whose value
 * is a randomly generated UUID. The Web service returns the new cookie as part
 * of the HTTP response message.
 */
@Path("/concerts")
public class ConcertResource {

    private static Logger LOGGER = LoggerFactory.getLogger(ConcertResource.class);
    private Map<Long, Concert> concerts = new ConcurrentHashMap<>();
    private long idCounter = 0;


    /**
     * Retrieves a concert based on it's unique id.
     * The response will have status code 200 on success, and 404 if the concert is not found.
     * @param id unique Id of the concert
     * @param clientId Cookie representing users clientId, If this isn't provided then one is generated and sent back.
     * @return A response object
     */
    @GET
    @Path("{id}")
    @Produces(SerializationMessageBodyReaderAndWriter.APPLICATION_JAVA_SERIALIZED_OBJECT)
    public Response retrieveConcert(@PathParam("id") long id, @CookieParam(Config.CLIENT_COOKIE) Cookie clientId) {
        LOGGER.info("Retrieving concert with id " + id + " from client:"+clientId);
        Concert concert = concerts.get(id);
        
        // Check if concert is null
        if (concert == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        return Response.ok(concert, SerializationMessageBodyReaderAndWriter.APPLICATION_JAVA_SERIALIZED_OBJECT_TYPE)
        		.cookie(makeCookie(clientId))
        		.build();
    }


    /**
     * Retrieves a collection of Concerts, where the "start" query
     * parameter identifies an index position, and "size" represents the
     * max number of successive Concerts to return. The HTTP response
     * message returns 200.
     * @param start Id of the first concert to retrieve
     * @param size The number of concerts to retrieve
     * @param clientId Cookie representing users clientId, If this isn't provided then one is generated and sent back.
     * @return A response object contains the ArrayList of concerts.
     */
    @GET
    @Produces(SerializationMessageBodyReaderAndWriter.APPLICATION_JAVA_SERIALIZED_OBJECT)
    public Response retrieveConcerts(
            @QueryParam("start") long start,
            @QueryParam("size") int size,
            @CookieParam(Config.CLIENT_COOKIE) Cookie clientId) {

        List<Concert> concertsCopy = new ArrayList<>();
        GenericEntity<List<Concert>> entity = new GenericEntity<List<Concert>>(concertsCopy) {};
        
        for (long i = start-1; i < start+size; i++) {
            Concert concert = concerts.get(i);
            if(concert != null) {
            	concertsCopy.add(concert);
            }
        }
        
        LOGGER.info("Retrieved " + concertsCopy.size() + " concerts for client: " + clientId);
        
        return Response.ok(entity, SerializationMessageBodyReaderAndWriter.APPLICATION_JAVA_SERIALIZED_OBJECT_TYPE)
        		.cookie(makeCookie(clientId))
        		.build();
    }


    /**
     * Creates a new Concert. The HTTP post message contains a
     * representation of the Concert to be created. The HTTP Response
     * message returns a Location header with the URI of the new Concert
     * and a status code of 201.
     * @param concert concert to create, Id is replaced with a server generated uniqueId.
     * @param clientId Cookie representing users clientId, If this isn't provided then one is generated and sent back.
     * @return A response object containing the location the resource( concert ) was created at.
     */
    @POST
    @Consumes(SerializationMessageBodyReaderAndWriter.APPLICATION_JAVA_SERIALIZED_OBJECT)
    public Response createConcert(Concert concert, @CookieParam(Config.CLIENT_COOKIE) Cookie clientId) {
        long id = idCounter++;
        Concert newConcert = new Concert(id, concert.getTitle(), concert.getDate());
        concerts.put(id, newConcert);

        LOGGER.info("Created Concert " + newConcert.toString() + "for client:" + clientId);

        return Response.created(URI.create("/concerts/" + newConcert.getId()))
        		.cookie(makeCookie(clientId))
        		.build();
    }


    /**
     * Deletes all Concerts, returning a status code of 204.
     * @param clientId Cookie representing users clientId, If this isn't provided then one is generated and sent back.
     * @return A No Content response object
     */
    @DELETE
    public Response deleteAllConcerts(@CookieParam(Config.CLIENT_COOKIE) Cookie clientId) {
        concerts.clear();
        idCounter = 0;
        LOGGER.info("Client "+clientId+" has deleted all concerts");
        return Response.noContent()
        		.cookie(makeCookie(clientId))
        		.build();
    }

    /**
     * Helper method that can be called from every service method to generate a
     * NewCookie instance, if necessary, based on the clientId parameter.
     *
     * @param clientId the Cookie whose name is Config.CLIENT_COOKIE, extracted
     *                 from a HTTP request message. This can be null if there was no cookie
     *                 named Config.CLIENT_COOKIE present in the HTTP request message.
     * @return a NewCookie object, with a generated UUID value, if the clientId
     * parameter is null. If the clientId parameter is non-null (i.e. the HTTP
     * request message contained a cookie named Config.CLIENT_COOKIE), this
     * method returns null as there's no need to return a NewCookie in the HTTP
     * response message.
     */
    private NewCookie makeCookie(Cookie clientId) {
        NewCookie newCookie = null;

        if (clientId == null) {
            newCookie = new NewCookie(Config.CLIENT_COOKIE, UUID.randomUUID().toString());
            LOGGER.info("Generated cookie: " + newCookie.getValue());
        }

        return newCookie;
    }
}
